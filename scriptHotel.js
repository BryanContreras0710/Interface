$(document).ready(function(){ 
    $.ajax({
            url: 'http://localhost:8080/hoteles',
            contentType: 'application/json',
            success: function(response) {
                var tbodyEl = $('#hot');

                tbodyEl.html('');

                response.forEach(function(hotel) {
                    tbodyEl.append('\
                        <tr>\
                            <td class="id">' + hotel.id_hotel + '</td>\
                            <td><input type="text" class="nombre" value="' + hotel.nom_hotel + '"></td>\
							<td><input type="text" class="direccion" value="' + hotel.direccion + '"></td>\
							<td><input type="text" class="habitacion" value="' + hotel.habitacion + '"></td>\
							<td><input type="text" class="telefono" value="' + hotel.telefono + '"></td>\
                            <td>\
                            <div class="btn-group">\
                            <button id="update-hotel" class="btn btn-info">Editar</button>\
                            <button id="delete-hotel" class="btn btn-danger">Borrar</button>\
                                </div>\
                            </td>\
                        </tr>\
                    ');
                });
            }
        });
    });

$(function() {
    // READ/GET
    $('#get-button-hoteles').on('click', function() {
        $.ajax({
            url: 'http://localhost:8080/hoteles',
            contentType: 'application/json',
            success: function(response) {
                var tbodyEl = $('#hot');

                tbodyEl.html('');

                response.forEach(function(hotel) {
                    tbodyEl.append('\
                        <tr>\
                            <td class="id">' + hotel.id_hotel + '</td>\
                            <td><input type="text" class="nombre" value="' + hotel.nom_hotel + '"></td>\
							<td><input type="text" class="direccion" value="' + hotel.habitacion+ '"></td>\
							<td><input type="text" class="habitacion" value="' + hotel.direccion + '"></td>\
							<td><input type="text" class="telefono" value="' + hotel.telefono + '"></td>\
                            <td>\
                            <div class="btn-group">\
                            <button id="update-hotel" class="btn btn-info">Editar</button>\
                            <button id="delete-hotel" class="btn btn-danger">Borrar</button>\
                                </div>\
                            </td>\
                        </tr>\
                    ');
                });
            }
        });
    });

    // CREATE/POST
    $('#create-form-hoteles').on('submit', function(event) {
        event.preventDefault();

        var info = { 
			nom_hotel: $('#nom_hotel').val(),
			direccion: $('#direccion').val(),
			habitacion: $('#habitacion').val(),
            telefono: $('#telefono').val(),			
		};

        $.ajax({
            url: 'http://localhost:8080/hotel/',
            method: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(info),
            success: function(response) {
                console.log(response);
                $('#nom_hotel').val(''),
				$('#direccion').val(''),
				$('#habitacion').val(''),
				$('#telefono').val(''),
                $('#get-button-hoteles').click();
            }
        });
    });

    // UPDATE/PUT
    $('#hoteles').on('click', '#update-hotel', function() {
        var rowEl = $(this).closest('tr');
        var id_hotel = rowEl.find('.id').text();
        var newName = rowEl.find('.nombre').val();
		var newDireccion = rowEl.find('.direccion').val();
		var newHabitacion = rowEl.find('.habitacion').val();
		var newTel = rowEl.find('.telefono').val();
		
		var info = {
            nom_hotel: newName,
            habitacion: newHabitacion,
			direccion: newDireccion,
			telefono: newTel,
		};

        $.ajax({
            url: 'http://localhost:8080/hotel/' + id_hotel,
            method: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify(info),
            success: function(response) {
                console.log(response);
                $('#get-button-hoteles').click();
            }
        });
    });

    // DELETE
    $('#hoteles').on('click', '#delete-hotel', function() {
        var rowEl = $(this).closest('tr');
        var id_hotel = rowEl.find('.id').text();

        $.ajax({
            url: 'http://localhost:8080/hotel/' + id_hotel,
            method: 'DELETE',
            contentType: 'application/json',
            success: function(response) {
                console.log(response);
                $('#get-button-hoteles').click();
            }
        });
    });
});