$(document).ready(function(){   
    $.ajax({
        url: 'http://localhost:8080/reservaciones',
        contentType: 'application/json',
        success: function(response) {
            var tbodyEl = $('#res');

            tbodyEl.html('');

            response.forEach(function(reservacion) {
                tbodyEl.append('\
               <tr>\
                    <td class="id">' + reservacion.id + '</td>\
                    <td><input type="text" class="Persona" value="' + reservacion.persona.nom_persona + '"></td>\
                    <td><input type="text" class="Hotel" value="' + reservacion.hotel.nom_hotel + '"></td>\
                    <td><input type="text" class="f_entrada" value="' + reservacion.f_entrada + '"></td>\
                    <td><input type="text" class="f_salida" value="' + reservacion.f_salida + '"></td>\
                    <td>\
                    <div class="btn-group">\
                    <button id="update-reser" class="btn btn-info">Editar</button>\
                    <button id="delete-reser" class="btn btn-danger">Borrar</button>\
                    </div>\
            </tr>\
                ');
            });
        }
    });
});

$(function() {
    // READ/GET
    $('#get-button-reservacion').on('click', function() {
        $.ajax({
            url: 'http://localhost:8080/reservaciones',
            contentType: 'application/json',
            success: function(response) {
                var tbodyEl = $('#res');

                tbodyEl.html('');

                response.forEach(function(reservacion) {
                    tbodyEl.append('\
                   <tr>\
                        <td class="id">' + reservacion.id + '</td>\
                        <td><input type="text" class="persona" value="' + reservacion.persona.nom_persona + '"></td>\
                        <td><input type="text" class="hotel" value="' + reservacion.hotel.nom_hotel + '"></td>\
                        <td><input type="text" class="f_entrada" value="' + reservacion.f_entrada + '"></td>\
                        <td><input type="text" class="f_salida" value="' + reservacion.f_salida + '"></td>\
                        <td>\
                        <div class="btn-group">\
                        <button id="update-reser" class="btn btn-info">Editar</button>\
                    <button id="delete-reser" class="btn btn-danger">Borrar</button>\
                        </div>\
                        </td>\
                </tr>\
                    ');
                });
            }
        });
    });

     // CREATE/POST
     $('#create-form-reservacion').on('submit', function(event) {
        event.preventDefault();
        var id1 = $("#id_persona").val();
        var id2 = $("#id_hotel").val();

        var persona ={
            id_persona:id1
        }

        var hotel ={
            id_hotel:id2
        }

        var info = {
            persona:persona,
            hotel:hotel,
            f_entrada: $('#f_entrada').val(),
            f_salida: $('#f_salida').val()

		};

        $.ajax({
            url: 'http://localhost:8080/reservacion/',
            method: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(info),
            success: function(response) {
                console.log(response);
                $('#id_persona').val(''),
			    $('#id_hotel').val(''),
                $('#f_entrada').val(''),
                $('#f_salida').val(''),
                $('#get-button-reservacion').click();
            }
        });
    });
     
    

    // UPDATE/PUT
    $('#reservacion').on('click', '#update-reser', function() {
        var rowEl = $(this).closest('tr');
        var id = rowEl.find('.id').text();
        var id3 = rowEl.find('.persona').text();
        var id4 = rowEl.find('.hotel').text();
        var newf_entrada = rowEl.find('.f_entrada').val();
        var newf_salida = rowEl.find('.f_salida').val();
        
        var persona ={
            id_persona:id1
        }

        var hotel ={
            id_hotel:id2
        }

        var info = {
            persona:persona,
            hotel:hotel,
            f_entrada: newf_entrada,
            f_salida: newf_salida

		};

        $.ajax({
            url: 'http://localhost:8080/reservacion/' + id,
            method: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify(info),
            success: function(response) {
                console.log(response);
                $('#get-button-reservacion').click();
            }
        });
    });

    // DELETE
    $('#reservacion').on('click', '#delete-reser', function() {
        var rowEl = $(this).closest('tr');
        var id = rowEl.find('.id').text();

        $.ajax({
            url: 'http://localhost:8080/reservacion/'+ id,
            method: 'DELETE',
            contentType: 'application/json',
            success: function(response) {
                console.log(response);
                $('#get-button-reservacion').click();
            }
        });
    });
});
