$(document).ready(function(){
    $.ajax({
        url: 'http://localhost:8080/personas',
        contentType: 'application/json',
        success: function(response) {
            var tbodyEl = $('#per');

            tbodyEl.html('');

            response.forEach(function(persona) {
                tbodyEl.append('\
                    <tr>\
                        <td class="id">' + persona.id_persona + '</td>\
                        <td><input type="text" class="nombre" value="' + persona.nom_persona + '"></td>\
                        <td><input type="text" class="apellido" value="' + persona.apellido + '"></td>\
                        <td><input type="text" class="edad" value="' + persona.edad + '"></td>\
                        <td>\
                        <div class="btn-group">\
                        <button id="update-persona" class="btn btn-info">Editar</button>\
                            <button id="delete-persona" class="btn btn-danger">Borrar</button>\
                        </div>\
                        </td>\
                    </tr>\
                ');
            });
        }
    });
});

$(function() {
    // READ/GET
    $('#get-button-personas').on('click', function() {
        $.ajax({
            url: 'http://localhost:8080/personas',
            contentType: 'application/json',
            success: function(response) {
                var tbodyEl = $('#per');

                tbodyEl.html('');

                response.forEach(function(persona) {
                    tbodyEl.append('\
                        <tr>\
                            <td class="id">' + persona.id_persona + '</td>\
                            <td><input type="text" class="nombre" value="' + persona.nom_persona + '"></td>\
                            <td><input type="text" class="apellido" value="' + persona.apellido + '"></td>\
                            <td><input type="text" class="edad" value="' + persona.edad + '"></td>\
                            <td>\
                            <div class="btn-group">\
                            <button id="update-persona" class="btn btn-info">Editar</button>\
                            <button id="delete-persona" class="btn btn-danger">Borrar</button>\
                            </div>\
                            </td>\
                        </tr>\
                    ');
                });
            }
        });
    });

    // CREATE/POST
    $('#create-form-personas').on('submit', function(event) {
        event.preventDefault();

        var info = {
			nom_persona: $('#nom_persona').val(),
			apellido: $('#apellido').val(),
			edad: $('#edad').val(),
		};

        $.ajax({
            url: 'http://localhost:8080/persona/',
            method: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(info),
            success: function(response) {
                console.log(response);
                $('#nom_persona').val(''),
			    $('#apellido').val(''),
				$('#edad').val(''),
                $('#get-button-personas').click();
            }
        });
    });

    // UPDATE/PUT
    $('#personas').on('click', '#update-persona', function() {
        var rowEl = $(this).closest('tr');
        var id_persona = rowEl.find('.id').text();
        var newName = rowEl.find('.nombre').val();
        var newLastName = rowEl.find('.apellido').val();
        var newAge = rowEl.find('.edad').val();
		
		var info = {
			nom_persona: newName,
			apellido: newLastName,
			edad: newAge,
		};

        $.ajax({
            url: 'http://localhost:8080/persona/' + id_persona,
            method: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify(info),
            success: function(response) {
                console.log(response);
                $('#get-button-personas').click();
            }
        });
    });

    // DELETE
    $('#personas').on('click', '#delete-persona', function() {
        var rowEl = $(this).closest('tr');
        var id_persona = rowEl.find('.id').text();

        $.ajax({
            url: 'http://localhost:8080/persona/'+ id_persona,
            method: 'DELETE',
            contentType: 'application/json',
            success: function(response) {
                console.log(response);
                $('#get-button-personas').click();
            }
        });
    });
});
